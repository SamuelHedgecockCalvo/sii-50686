#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "Raqueta.h"

int main()
{
	//Se crea el puntero a la memoria compartida y para la proyeccion
	DatosMemCompartida* memoriap;
	char* proyeccion;
	//Realizamos la apertura del fichero
	int fdfile;
	fdfile = open("/tmp/Botdata.txt",O_RDWR);
	//Se realiza la proyeccion el fichero
	proyeccion = (char*)mmap(NULL,sizeof(*(memoriap)),PROT_WRITE|PROT_READ,MAP_SHARED,fdfile,0);
	//Despues de la proyeccion cerramos el fichero
	close(fdfile);
	//Se asigna la direccion de comienzo de la region creada al puntero tipo DatosMemCompartida
	memoriap=(DatosMemCompartida*)proyeccion;
	//Acciones de control de la raqueta
	while(1)
	{
		usleep(25000);  
		float posRaqueta;
		float posRaquetaTO;
		posRaqueta=((memoriap->raqueta1.y2+memoriap->raqueta1.y1)/2);
		//posRaquetaTO=((memoriap->raquetatimeout.y2+memoriap->raquetatimeout.y1)/2);
		//En funcion de la posicion de la esfera moveremos la raqueta
		if(posRaqueta<memoriap->esfera.centro.y)
			memoriap->accion=1;
		else if(posRaqueta>memoriap->esfera.centro.y)
			memoriap->accion=-1;
		else
			memoriap->accion=0;
		//if(posRaquetaTO<memoriap->esfera.centro.y)
		//	memoriap->acciontimeout=1;
		//else if(posRaquetaTO>memoriap->esfera.centro.y)
		//	memoriap->acciontimeout=-1;
		//else
		//	pMemComp->acciontimeout=0;
	}
	//Una vez terminado el bucle podremos desmontar la proyeccion
	munmap(proyeccion,sizeof(*(memoriap)));
}
